USE [TEST_DEV]
GO
/*
drop table  [dbo].[FTITNOM_];

CREATE TABLE [dbo].[FTITNOM_](
	[CCLATIT] numeric(8) NULL,
	[CCLANOM] numeric(4) NULL,
	[CTIPONOM] [nvarchar](1) NULL,
	[CNOMTIT] [nvarchar](40) NULL,
	[CCLAALF] [nvarchar](20) NULL,
	[CLONAP1] numeric(2) NULL,
	[CLONAP2] numeric(2) NULL,
	[CLONNOM] numeric(2) NULL,
	[CNIVELSEG] [nvarchar](1) NULL
) ON [PRIMARY]


insert into [dbo].[FTITNOM_] select * from [dbo].[FTITNOM]*/

/*
drop table [dbo].[FTITDOM_]

CREATE TABLE [dbo].[FTITDOM_](
	[CCLATIT] numeric(8) NULL,
	[CCLADOM] numeric(4) NULL,
	[CTIPODOM] [nvarchar](1) NULL,
	[CTIPCALLE] [nvarchar](2) NULL,
	[CDOMICI] [nvarchar](40) NULL,
	[CPOBLAC] [nvarchar](30) NULL,
	[CPOSTAL] numeric(5) NULL,
	[CPAIS] [nvarchar](3) NULL,
	[CTELEF] numeric(9) NULL,
	[CFAX] numeric(9) NULL,
	[CTIPOVIA] [nvarchar](5) NULL,
	[CNOMVIA] [nvarchar](30) NULL,
	[CTIPNUM] [nvarchar](3) NULL,
	[CNCASA] numeric(4) NULL,
	[CCALIFIC] [nvarchar](3) NULL,
	[CBLOQUE] [nvarchar](3) NULL,
	[CPORTAL] [nvarchar](3) NULL,
	[CESCALERA] [nvarchar](3) NULL,
	[CPISO] [nvarchar](3) NULL,
	[CPUERTA] [nvarchar](3) NULL,
	[CCOMPLEMN] [nvarchar](40) NULL,
	[CMUNICIPIO] [nvarchar](30) NULL,
	[CCODMUN] numeric(9) NULL,
	[CFILLER] [nvarchar](80) NULL
) ON [PRIMARY]

GO


insert into [dbo].[FTITDOM_] select * from [dbo].[FTITDOM]
*/



drop table [dbo].[FTITPRL_]

CREATE TABLE [dbo].[FTITPRL_](
	[CCLATIT] numeric(8) NULL,
	[CTIPODNI] [nvarchar](1) NULL,
	[CCLADNI] [nvarchar](10) NULL,
	[CFCCREA] numeric(7) NULL,
	[CFCNAC] numeric(7) NULL,
	[CCLAPRF] numeric(4) NULL,
	[CSEXO] [nvarchar](1) NULL,
	[CCATCMIFID] [nvarchar](1) NULL,
	[CCLAFIL] numeric(4) NULL,
	[CCLACACL] numeric(4) NULL,
	[CNIVELSEG] [nvarchar](1) NULL,
	[CNUMCON] numeric(3) NULL,
	[CNUMNOM] numeric(3) NULL,
	[CNUMDOM] numeric(3) NULL,
	[CNUMCORR] numeric(3) NULL,
	[CNUMVINC] numeric(3) NULL,
	[CNUMTEX] numeric(3) NULL,
	[CCHKDNI] [nvarchar](1) NULL,
	[CCNAE] [nvarchar](5) NULL,
	[CSWFOTO] [nvarchar](1) NULL,
	[CPAISRES] [nvarchar](3) NULL,
	[CIDIOMA] numeric(1) NULL,
	[CSITLEGAL] [nvarchar](1) NULL,
	[CTIPORES] [nvarchar](1) NULL,
	[CTIPONAC] [nvarchar](1) NULL,
	[CSITUACIO] [nvarchar](1) NULL,
	[CNUMMED] numeric(3) NULL,
	[CMINUS] [nvarchar](1) NULL,
	[CPERMIN] numeric(4) NULL,
	[CCLIVIP] [nvarchar](1) NULL,
	[CORIGEN] [nvarchar](2) NULL,
	[CFCULACT] numeric(7) NULL,
	[CNIVCRIE] [nvarchar](1) NULL,
	[CCATCMED] [nvarchar](1) NULL,
	[CNONGRATO] [nvarchar](1) NULL,
	[CSWPROC1] [nvarchar](1) NULL,
	[CSWPROC2] [nvarchar](1) NULL,
	[CCLATITF] numeric(8) NULL,
	[CTIPBLK] [nvarchar](1) NULL,
	[CMOTBLK] [nvarchar](2) NULL,
	[CESTTRAH] [nvarchar](2) NULL,
	[CFCUMOD] numeric(7) NULL,
	[FILLER] [nvarchar](194) NULL
) ON [PRIMARY]

GO

insert into [dbo].[FTITPRL_] select * from [dbo].[FTITPRL]